var RegexReplacer = require('regexp-sourcemaps');
var applySourceMap = require('vinyl-sourcemaps-apply');
var path = require('path');

module.exports = function(file, options) {

	options = options || {};

	addCommonJsWrapper(file, options);

	// Add sourcemaps if the sourcemaps plugin is active
	if (file.sourceMap) {
		applySourceMap(file, result.map);
	}

};

function addCommonJsWrapper(file, options) {

	var relativePath = makeRelativePath(file, options.relativePath);

	var replacer = new RegexReplacer(
		/^((.|\n)*)$/,
		wrapperExpression(modulePath(relativePath))
	);

	var result = replacer.replace(file.contents, relativePath);
	file.contents = new Buffer(result.code);

}

function wrapperExpression(modulePath) {
	return 'require.register("' + modulePath + '", function(exports, require, module) { $1 });';
}

function makeRelativePath(file, relativePath) {
	var rootPath = path.join(process.cwd(), relativePath || '.');
	return path.relative(rootPath, file.path);
}

function modulePath(relativePath) {
	return relativePath.replace('.js', '');
}
