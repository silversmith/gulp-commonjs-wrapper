'use strict';

var wrapFile = require('../wrap-file');
var assert = require('assert');
var path = require('path');

describe('wrap-file', function() {

	it('should wrap a simple .js file with a commonjs wrapper', function() {
		
		var file = {
			contents: 'console.log("hi world!");',
			path: path.join(process.cwd(), 'index.js')
		};

		wrapFile(file);

		var expectedWrappedContent = 'require.register("index", function(exports, require, module) { console.log("hi world!"); });';
		assert.equal(file.contents.toString('UTF-8'), expectedWrappedContent);
	});

});