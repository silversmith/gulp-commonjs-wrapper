'use strict';

var assert = require('assert');
var path = require('path');
var requireFn = require('../require');

describe('require', function() {

	beforeEach(function() {
		requireFn.modules = [];
	});

	it('should allow requiring a registered module', function() {

		requireFn.register('index', function(exports, module, require) {
			module.exports = 'index';
		});

		assert.equal(requireFn('index'), 'index');

	});

	it('should throw an error if path not defined, null, or empty', function() {

		assert.throws(function() {
			requireFn();
		}, /path is required/);

		assert.throws(function() {
			requireFn('');
		}, /path is required/);

		assert.throws(function() {
			requireFn(null);
		}, /path is required/);

	});

	it('should throw an error if registering a module path that is already registered', function() {
		
		requireFn.register('index', function(exports, module, require) {
			module.exports = 'index';
		});

		assert.throws(function() {

			requireFn.register('index', function(exports, module, require) {
				module.exports = 'index';
			});		

		}, /Module already defined/);

	});

	it('should allow requiring a module from a required module', function() {

		requireFn.register('index', function(exports, module, require) {
			module.exports = 'index';
		});

		assert.equal(requireFn('index'), 'index');

	});

	it('should allow using exports', function() {

		requireFn.register('index', function(exports, module, require) {
			module.exports.name = 'index';
		});

		assert.equal(requireFn('index').name, 'index');

	});

	it('should allow using relative paths', function() {

		requireFn.register('person', function(exports, module, require) {
			module.exports = 'person';
		});

		requireFn.register('index', function(exports, module, require) {			
			module.exports = {name: 'index', person: require('./person')};
		});

		requireFn.register('cool/index', function(exports, module, require) {
			module.exports = {name: 'cool/index', person: require('../person')};
		});

		requireFn.register('l1/l2/l3/index', function(exports, module, require) {
			module.exports = {name: 'level/index', person: require('../../../person')};
		});

		assert.equal(requireFn('index').person, 'person');
		assert.equal(requireFn('cool/index').person, 'person');
		assert.equal(requireFn('l1/l2/l3/index').person, 'person');
	});

});
