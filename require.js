
function require(path, requestingPath) {	
	if (!path) throw new Error('path is required');

	path = require.resolvePath(path, requestingPath);
	require.ensureModule(path, requestingPath);
	return require.modules[path].exports;
}

require.modules = {};

require.register = function(path, moduleFn) {	
	if (!path) {
		throw new Error('require.register: You must provide a valid module path');
	}
	if (require.modules[path]) {
		throw new Error('Module already defined: ' + path);
	}	
	
	require.modules[path] = {
		name: path,
		exports: {},
		initializer: moduleFn,
		initialized: false
	};

};

require.resolvePath = function(path, requestingPath) {

	function baseDir(path) {
		if (!path) return '';

		var parts = path.split('/');
		parts.pop();
		return parts.join('/');
	}	

	function fullPath(dirs, parts) {
		var fullPath = dirs.concat(parts).join('/');
		if (fullPath[0] == '/') {
			fullPath = fullPath.substring(1);
		}
		return fullPath;
	}

	function resolveRelative(path, startDir) {
		var dirs = startDir.split('/');
		var parts = path.split('/');
		
		var part;
		
		while(part = parts.shift()) {
			if (part === '.') {
				// do nothing, we are at the right level
			} else if (part === '..') {
				dirs.pop();
			} else {
				parts.unshift(part);
				return fullPath(dirs, parts);
			}		
		}

	}

	return resolveRelative(path, baseDir(requestingPath));
};

require.ensureModule = function(path, requestingPath) {

	var module = require.modules[path];

	if (!module) {
		throw new Error('Could not find ' + path + ' from ' + (requestingPath || '(root)'));
	}

	if (!module.initialized) {
		require.initializeModule(module);
	}

};

require.initializeModule = function(module) {
	
	var relativeRequire = function(path) { 
		return require(path, module.name); 
	};

	module.initialized = true;
	module.initializer(module.exports, module, relativeRequire);
};

if (typeof module != "undefined") {
	module.exports = require;
}
