var through = require('through2');
var wrapFile = require('./wrap-file');
var wrapLibrary = require('./wrap-library');

function wrapFile(options) {

	function transform(file, encoding, callback) {

		wrapFile(file, options);
		
		callback(null, file);
	};

	return through.obj(transform);
};

function wrapLibrary(options) {

	function transform(file, encoding, callback) {

		wrapLibrary(file, options);
		
		callback(null, file);
	};

	return through.obj(transform);
}

module.exports = {
	wrapFile: wrapFile,
	wrapLibrary: wrapLibrary
};
